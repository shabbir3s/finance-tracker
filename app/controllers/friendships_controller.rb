class FriendshipsController < ApplicationController
    def destroy
        @friendship = current_user.friendships.where(friend_id: params[:id]).first
        @friendship.destroy
        
        flash[:notice] = "Friend was removed"
    
        respond_to do |wants|
            wants.html { redirect_to(my_friends_path) }
            wants.xml  { head :ok }
        end
    end
end